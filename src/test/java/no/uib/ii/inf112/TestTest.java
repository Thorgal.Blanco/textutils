package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			if (width%2==0 && width > text.length())
				return " ".repeat(extra) + text + " ".repeat(extra) + " ";
			
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = (width - text.length());
			if (extra < 0)
				return text;
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = (width - text.length());
			if (extra < 0)
				return text;
			return text + " ".repeat(extra) ;
		}

		public String justify(String text, int width) {
			int extra = width/text.length();
			String mem = "";
			
			List<E> words = text.split(" ");
			System.out.println(words);
			
			for(String word : words) {
				if(word == words.get(0))
					//ettelleranna
				if (word == words.get(words.length()-1))
					//
					
				mem
				
				mem =
			}
			
			return mem;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals("ABC", aligner.center("ABC", 2));
		assertEquals("  A   ", aligner.center("A", 6));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
	
	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("ABC", aligner.flushRight("ABC", 2));
		
	}
	
	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("ABC", aligner.flushLeft("ABC", 2));
	}
	
	@Test
	void testJustify() {
		assertEquals("fee   fai   foe", aligner.justify("fee fai foe", 15));
		assertEquals("fee    fai   foe", aligner.justify("fee fai foe", 16));
	}
}
